#!/usr/bin/env python
'''
CLEESE toolbox v1.0
mar 2018, J.J. Burred <jjburred@jjburred.com> for IRCAM/CNRS

Example configuration script.
'''
import os

# main parameters
main_pars = {
    'outPath': os.getcwd() + '/../tmp/',   # output root folder
    'numFiles': 1 ,     # number of output files to generate (for random modifications)
    'chain': False,
    #'transf': ('stretch','pitch','eq','gain')   # modifications to apply
    'transf': ['pitch']
}

# global analysis parameters
ana_pars = {
    'anaWinLen':   0.04,    # analysis window length in s (not to be confused with the BPF processing window lengths)
    'oversampling':   8,    # number of hops per analysis window
    # TODO missing:
    # st_pars.svp.b_preserveTransients = 1;
    # st_pars.svp.b_normalizeOutput    = 1;
}

# parameters for random pitch countours
pitch_pars = {
    'winLen': 0.12,    # pitch transposition window in seconds. If 0 : static transformation
    'numWin': 4,       # number of pitch transposition windows. If 0 : static transformation
    'winUnit': 'n',    # 's': force winlength in seconds,'n': force number of windows (equal length)
    'std': 267,        # standard deviation (cents) for random transposisiton (Gaussian distrib for now)
    'trunc': 2.2,        # truncate distribution values (factor of std)
    'BPFtype': 'ramp', # type of breakpoint function:
                       #      'ramp': linear interpolation between breakpoints
                       #      'square': square BPF, with specified transition times at edges
    'trTime': 0.02  # in s: transition time for square BPF
    # TODO missing
    # st_pars.pitch.b_preserveEnvelope = 1;   % spectral envelope preservation
}

pars = {
    'main_pars': main_pars,
    'ana_pars': ana_pars,
    'pitch_pars': pitch_pars
}

def get_pitch_pars():
    return pitch_pars
