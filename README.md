# Pitch Gibbs sampler
This is a very naive way to build a Gibbs sampler on pitch points.

Todo:
- [ ] Check if the info's contain all important information
- [ ] Preload audio
- [ ] Make it easier to add new recordings (instead of only `Bier`) to the experiment
- [ ] Improve the implementation for more or less pitch points
- [ ] @Peter, randomise the starting point of the slider on each free trial
- [ ] I probably missed many points