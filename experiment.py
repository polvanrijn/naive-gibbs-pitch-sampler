# pylint: disable=unused-import,abstract-method,unused-argument

##########################################################################################
#### Imports
##########################################################################################

from flask import Markup
import random
from typing import Union, List
import time
from math import ceil
from dallinger import db
from flask import Response
import json

import dlgr_utils.experiment
from dlgr_utils.timeline import (
    InfoPage,
    Timeline,
    SuccessfulEndPage,
    ResponsePage
)
from dlgr_utils.trial.chain import ChainNetwork
from dlgr_utils.trial.gibbs_sampler import (
    GibbsTrial, GibbsNode, GibbsSource, GibbsTrialGenerator
)

import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__)

import rpdb

NUM_POINTS = 5
RANGE = ceil(267 * 2.2)
NUM_STEPS = 100
STEP_SIZE = (2 * RANGE) / NUM_STEPS
TARGETS = ['critical', 'suggestive']
PITCH_POINTS = list(range(1, (NUM_POINTS + 1)))


class AudioSliderPage(ResponsePage):
    def __init__(
            self,
            label: str,
            prompt: Union[str, Markup],
            selected: str,
            starting_values: List[int],
            time_allotted=None
    ):
        assert selected in PITCH_POINTS
        self.prompt = prompt
        self.selected = selected
        self.starting_values = starting_values

        super().__init__(
            time_allotted=time_allotted,
            template_path="templates/audio-slider.html",
            label=label,
            template_arg={
                "prompt": prompt,
                "selected": selected,
                "pitch_points": starting_values,
                "point_idxs": list(range(NUM_POINTS)),
                "step": STEP_SIZE,
                "range": RANGE
            }
        )

    def compile_details(self, response, answer, metadata, experiment, participant):
        # pylint: disable=unused-argument
        return {
            "prompt": self.prompt,
            "selected": self.selected,
            "initial_values": self.starting_values
        }


class CustomTrial(GibbsTrial):
    __mapper_args__ = {"polymorphic_identity": "custom_trial"}

    @property
    def target(self):
        return self.source.target

    @property
    def prompt(self):
        return (Markup(
            "Adjust the slider to make the recording sound most "
            f"<strong>{self.target}</strong>"
        ))

    def show_trial(self, experiment, participant):
        selected_pitch_point = PITCH_POINTS[self.active_index]

        return AudioSliderPage(
            "pitch_point_trial",
            self.prompt,
            selected=selected_pitch_point,
            starting_values=self.initial_vector,
            time_allotted=5
        )


class CustomNode(GibbsNode):
    __mapper_args__ = {"polymorphic_identity": "custom_node"}


class CustomSource(GibbsSource):
    __mapper_args__ = {"polymorphic_identity": "custom_source"}

    def generate_seed(self, network, experiment, participant):
        return {
            "active_index": random.randint(0, len(set(TARGETS))),
            # TODO check if this the best distribution
            "vector": [random.normalvariate(0, RANGE) for _ in PITCH_POINTS]
        }

    # TODO I am not sure if I fully understand this part...
    @property
    def target(self):
        network = self.network
        if network.chain_type == "across":
            index = network.id
        elif network.chain_type == "within":
            index = network.id_within_participant
        else:
            raise ValueError(f"Unidentified chain type: {network.chain_type}")
        return TARGETS[index % len(TARGETS)]


trial_generator = GibbsTrialGenerator(
    trial_class=CustomTrial,
    node_class=CustomNode,
    source_class=CustomSource,
    phase="experiment",
    time_allotted_per_trial=5,
    chain_type="within",
    num_trials_per_participant=20,
    num_nodes_per_chain=5,
    num_chains_per_participant=5,
    num_chains_per_experiment=None,
    trials_per_node=1,
    active_balancing_across_chains=True,
    check_performance_at_end=False,
    check_performance_every_trial=False,
    propagate_failure=False,
    recruit_mode="num_participants",
    target_num_participants=10,
    async_post_trial="dlgr_utils.demos.gibbs_sampler.experiment.async_post_trial",
    async_post_grow_network="dlgr_utils.demos.gibbs_sampler.experiment.async_post_grow_network"
)


def async_post_trial(trial_id):
    logger.info("Running async_post_trial for trial %i...", trial_id)
    trial = CustomTrial.query.filter_by(id=trial_id).one()
    time.sleep(1000)
    trial.awaiting_process = False
    db.session.commit()


def async_post_grow_network(network_id):
    logger.info("Running async_post_grow_network for network %i...", network_id)
    network = ChainNetwork.query.filter_by(id=network_id).one()
    time.sleep(0)
    network.awaiting_process = False
    db.session.commit()


##########################################################################################
#### Experiment
##########################################################################################

# Weird bug: if you instead import Experiment from dlgr_utils.experiment,
# Dallinger won't allow you to override the bonus method
# (or at least you can override it but it won't work).
class Exp(dlgr_utils.experiment.Experiment):
    timeline = Timeline(
        trial_generator,
        InfoPage("You finished the experiment!", time_allotted=0),
        SuccessfulEndPage()
    )

    def __init__(self, session=None):
        super().__init__(session)
        self.initial_recruitment_size = 1


extra_routes = Exp().extra_routes()


@extra_routes.route("/synthesize/<pitch_points>/", methods=["GET", "POST"])
def synthesize(pitch_points):
    import os
    pitch_points = [float(p) for p in pitch_points.split(',')]
    if len(pitch_points) != NUM_POINTS:
        return Response('Number of pitch points sent to server (%d) must be same as NUM_POINTS (%d)' % (
            len(pitch_points), NUM_POINTS), status=203, mimetype="application/json")

    if any([abs(p) > RANGE for p in pitch_points]):
        return Response('All pitch points sent to the server must be within the predifined RANGE: %d' % RANGE,
                        status=203, mimetype="application/json")

    filename = 'bier_' + '+'.join([str(x) for x in pitch_points]).replace('.', '_') + '.wav'

    folder_server = 'static/stimuli/'
    if not os.path.exists(folder_server):
        os.mkdir(folder_server)

    file_path = folder_server + filename
    if os.path.exists(file_path):
        logger.info("File %s already exists" % filename)
    else:
        from .libs import cleese
        import numpy as np
        import scipy.io.wavfile as wav
        pitch_points = np.array(pitch_points)
        # TODO eventually change stimulus type at some point
        input_file = 'bier.wav'
        input_path = 'libs/' + input_file
        cfg_file = 'libs/cleese/cleeseConfig.py'

        if input_file == "bier.wav":
            time_stamps = np.array([0, 0.090453, 0.18091, 0.27136, 0.36181])
        else:
            raise Exception('Sound file not supported')

        if len(time_stamps) != len(pitch_points):
            raise Exception('Number of time steps must be equal to the number of pitch points!')

        given_BPF = np.column_stack((time_stamps, pitch_points))

        # Read the base file
        waveIn, sr = cleese.wavRead(input_path)

        # Synthesize it
        waveOut, BPFout = cleese.process(soundData=waveIn, configFile=cfg_file, sr=sr, BPF=given_BPF)
        # I had issues with playing wavs in my version of Google Chrome
        # TODO just generate the mp3 first
        wav.write(file_path, sr, waveOut)
        old_file_path = file_path
        file_path = file_path.replace('.wav', '.mp3')
        os.system('sox -t wav -c 1 %s -t mp3 %s' % (old_file_path, file_path))

    return json.dumps({'filename': '/' + file_path})
